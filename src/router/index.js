import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import {useDefaultStore} from "@/stores";
import LoginUser from "@/components/LoginUser.vue";
import StoresView from "@/views/StoresView.vue";
import DetailProduct from "@/components/DetailProduct.vue";
import UpdatePassword from "@/components/UpdatePassword.vue";
import EditProduct from "@/components/addItem/EditProduct.vue";



const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  linkActiveClass: 'active',
  ScrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    return { top: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginUser
    },
    {
      path: '/logout',
      name: 'logout',
      component: HomeView
    },
    {
      path: '/register',
      name: 'register ',
      component: () => import('../components/RegisterUser.vue')
    },
    {
      path: '/services',
      name: 'services',
      component: ()=>import('../views/ServicesView.vue')
    },
    {
      path: '/stores',
      name: 'store',
      component: StoresView
    },
    {
        path: '/stores/:id',
        name: 'storeGestion',
        component: ()=>import('../components/StoreGestion.vue')
    },
    {
      path: '/product/:id',
      name: 'product',
      component: DetailProduct,
      children:[
        {
            path: 'contact',
            name: 'contact',
            component: ()=>import('../components/ContactOwner.vue')
        }
      ]
    },
    {
      path: '/profile',
      name: 'profile',
      component: ()=>import('../views/Profile-user.vue'),

    },
    {
      path: '/profile/:id/stores',
      name: 'boutiquesUser',
      component: ()=>import('../views/StoresUserView.vue'),
    },
    {
      path: '/addstore',
      name: 'addstore',
      component: ()=>import('../components/addItem/AddStore.vue')
    },
    {
      path: '/addproduct/:id/:owner',
      name: 'addproduct',
      strict: true,
      beforeEnter: (to, from, next) => {
        if (to.params.owner === false) {
          next({ name: '/stores' })
        } else {
          next()
        }
      },
      component: ()=>import('../components/addItem/AddProduct.vue'),

    },
    {
      path: '/addcategory',
      name: 'addcategory',
      component: ()=>import('../components/addItem/AddCategory.vue')
    },
    {
      path: '/changepwd',
      name: 'changepwd',
      component:UpdatePassword
    },
    {
      path: '/edit',
      name: 'edit',
      component:EditProduct
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
router.beforeEach((to, from, next) => {
  const store = useDefaultStore()
  // on redirige sur la page login si on n'est pas authentifié...
  const publicPages = ['/','/login', '/about', '/register','/changepwd'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = store.connecte;

  if (authRequired && !loggedIn) {
    return next('/login');
  }
  next();
})
