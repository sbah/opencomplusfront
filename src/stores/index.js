
import { defineStore } from 'pinia'
import Axios from '@/api/axios'

export const useDefaultStore = defineStore({
    id: 'product',
    state: () => ({
        allProducts: [],
        allStores: [],
        productValue: {},
        storeValue: {},
        storesUser: [],
        connecte: false,
        notConnecte: true,
        login: '',
        user:{
            id:null,
            myProducts: [],
        },
        token: '',
    }),
    actions: {
        getAllStores(isloading,listeStore) {
            Axios().get(`stores`)
                .then(response => {
                        return response.data['hydra:member'];
                    }
                ).then((data) => {
                    let tab=[]
                    this.allStores = data;
                    this.allProducts = data.consumableProducts;
                    listeStore.value = data
                isloading.value=false
                for (let i = 0; i < this.storesUser.length; i++) {
                    tab.push(data[i].consumableProducts);
                }
                this.allProducts = tab.flat();
                    isloading.value=false
                }
            )
        },
        initialiseStore() {
            if(localStorage.getItem('store')) {
                this.$state =  JSON.parse(localStorage.getItem('store'))
            }
        },
        saveStore() {
            localStorage.setItem('store', JSON.stringify(this.$state))
        },
        getAllStoreByUser(id,isloading,listeStore) {
            Axios().get(`stores?page=1&user=${id}`)
                .then(response => {
                        return response.data['hydra:member'];
                    }
                ).then((data) => {
                    this.storesUser = data;
                    this.allStores = data;
                    this.allProducts = data.consumableProducts;
                    listeStore.value = data
                    isloading.value=false
                    console.log(this.storesUser)
                    this.allProduct = data.consumableProducts;
                    console.log(data.description)
                }
            )
        },
        setConnecte(connect, log, token) {
            this.connecte = connect
            this.login = {id:log.id, name:log.name,roles:log.roles, email:log.email,address:log.address}
            this.token = token
            this.notConnecte = !connect
            sessionStorage.setItem('connecte', connect)
            sessionStorage.setItem('login', JSON.stringify(this.login))
            sessionStorage.setItem('token', token)
        },
        setConnecteWithSessionStorage() {
            if (sessionStorage.getItem('connecte') && sessionStorage.getItem('login') && sessionStorage.getItem('token')) {
                this.connecte = sessionStorage.getItem('connecte')
                this.login = JSON.parse(sessionStorage.getItem('login'))
                this.token = sessionStorage.getItem('token')
                this.notConnecte = !this.connecte
            }
        },
        setNotConnecte(connect) {
            this.connecte = connect
            this.notConnecte = !connect
            this.login = ''
            this.token = ''
            sessionStorage.setItem('connecte', connect)
            sessionStorage.setItem('login', '')
            sessionStorage.setItem('token', '')
            sessionStorage.removeItem('connecte')
            sessionStorage.removeItem('login')
            sessionStorage.removeItem('token')
        },
        getStoreUserByAxios(id,stores) {
            Axios()
            Axios().get(`stores?page=1&user=${id}`)
                .then(response => {
                        return response.data['hydra:member'];
                    }
                ).then((data) => {
                    this.storeUser = data;
                    stores.value = data

                }
            )
        }
    }


})
