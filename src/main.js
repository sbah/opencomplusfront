import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'



//FontAwesome
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import './assets/main.css'
//FORMKIT
import { plugin, defaultConfig } from '@formkit/vue'
import '@formkit/themes/genesis'
import '@formkit/addons/css/multistep'
import {createMultiStepPlugin} from "@formkit/addons";

const app = createApp(App)
app.use(createPinia())
app.use(router)

app.use(plugin, defaultConfig({
    'theme': 'genesis',
    plugins: [createMultiStepPlugin()],
}))
app.mount('#app')
