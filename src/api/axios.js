import axios from 'axios'
import {useDefaultStore} from "@/stores";


export default() => {
    const store = useDefaultStore()
    return axios.create({
        baseURL: 'https://localhost:8000/api/',
        withCredentials: false,
        headers: {
            'Accept': 'application/ld+json',
            'Content-Type': 'application/ld+json',
            'Authorization': 'Bearer ' + store.token
        }
    })
}
